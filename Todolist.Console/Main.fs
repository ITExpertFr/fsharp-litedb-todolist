﻿module Todolist.Console.Main

open System
open System.Collections.Generic
open LiteDB
open Todolist.Console.Shared
open Todolist.Console.Command
open Todolist.Console.ValidationTypes
let defaultConsoleArguments = {
  Command = List
  TodoId = 0
  TodoTitle = ""
}

let isInt (str:string) = fst (Int32.TryParse(str))

let makeConsoleArgumentsWithId (args:string[]) command =
  if (args.Length < 2 || not (isInt args.[1])) then
    Failure ConsoleArgumentsCouldNotBeMade
  else
    Success { defaultConsoleArguments with Command = command; TodoId = Int32.Parse(args.[1]) }

let makeConsoleArgumentsWithTitle (args:string[]) command =
  if (args.Length < 2) then
    Failure ConsoleArgumentsCouldNotBeMade
  else
    Success { defaultConsoleArguments with Command = command; TodoTitle = args.[1] }

let makeConsoleArgumentsWithIdAndTitle (args:string[]) command =
  if (args.Length < 3 || not (isInt args.[1])) then
    Failure ConsoleArgumentsCouldNotBeMade
  else
    Success { Command = command; TodoTitle = args.[2]; TodoId = Int32.Parse(args.[1]) }

let makeConsoleArgumentsWithoutValidation command =
  Success { defaultConsoleArguments with Command = command }

let argsToConsoleArguments (args:string[]) =
  if args.Length = 0 then
    Failure ConsoleArgumentsNoArgumentSpecified
  else
    match args.[0] with
    | "add" -> makeConsoleArgumentsWithTitle args Add
    | "remove" -> makeConsoleArgumentsWithId args Remove
    | "update" -> makeConsoleArgumentsWithIdAndTitle args Update
    | "complete" -> makeConsoleArgumentsWithId args Complete
    | "list" -> makeConsoleArgumentsWithoutValidation List
    | "all" -> makeConsoleArgumentsWithoutValidation All
    | "get" -> makeConsoleArgumentsWithId args Get
    | _ -> Failure (ConsoleArgumentsCommandDoesNotExist args.[0])

let todoToString (todo:Model.Todo) =
  let todoToString = $"{todo.Id}. {todo.Title} ({todo.CreationDate})"
  match todo.Status with
  | Model.Ongoing -> todoToString
  | Model.Done date -> $"{todoToString} - Done ({date})"

let todoListToString (todos:Model.Todo list) =
  todos
  |> List.map todoToString
  |> List.reduce (fun a b -> $"{a}\n{b}")

let messageToString (errorMessage:Message) =
  let error =
    match errorMessage with
    | DatabaseMappingFailed -> "Exception encountered while mapping database result"
    | DatabaseEntryDoesntExists -> "Requested entry doesn't exist"
    | DatabaseEntryIsInvalid -> "Invalid data, couldn't fetch from the database"
    | DbmsDeleteError -> "Error during DELETE operation"
    | DbmsUpdateError -> "Error during UPDATE operation"
    | ConsoleArgumentsNoArgumentSpecified -> "No argument specified"
    | ConsoleArgumentsCommandDoesNotExist cmd -> $"Requested command \"{cmd}\" doesn't exist"
    | ConsoleArgumentsCouldNotBeMade -> "Invalid arguments"

  $"ERROR: {error}"

let getResult result =
  match result with
  | Success s -> todoListToString s
  | Failure f -> messageToString f
