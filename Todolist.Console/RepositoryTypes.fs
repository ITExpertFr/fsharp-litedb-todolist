﻿module Todolist.Console.RepositoryTypes

open Shared
open Todolist.Console.Model
open Todolist.Console.ValidationTypes

type ITodoRepository = {
  get : int -> Result<Todo, Message>
  getAll : Result<Todo list, Message>
  getOngoing : Result<Todo list, Message>
  add : string -> Result<Todo, Message>
  remove : int -> Result<Todo, Message>
  updateTitle : int -> string -> Result<Todo, Message>
  updateStatus : int -> Result<Todo, Message>
}
