﻿module Todolist.Console.Model

open System

type TodoStatus =
  | Ongoing
  | Done of DateTime

type Todo = {
  Id: int
  Title: string
  Status: TodoStatus
  CreationDate: DateTime
}
