﻿module Todolist.Console.Program

open System
open Todolist.Console.Shared

[<EntryPoint>]
let main argv =
  Main.argsToConsoleArguments argv
  |> bind Command.execute
  |> Main.getResult
  |> Console.WriteLine
  0
