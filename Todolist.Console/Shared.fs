﻿module Todolist.Console.Shared

///<summary>Two-track type</summary>
type Result<'TSuccess, 'TFailure> =
  | Success of 'TSuccess
  | Failure of 'TFailure

///<summary>Convert a single value into a two-track result</summary>
let succeed x =
  Success x

///<summary>Convert a single value into a two-track result</summary>
let fail x =
  Failure x

///<summary>Apply either a success function or failure function</summary>
let either successFunc failureFunc twoTrackInput =
  match twoTrackInput with
  | Success s -> successFunc s
  | Failure f -> failureFunc f

///<summary>Convert a switch function into a two-track function</summary>
let bind f =
  either f fail

//<summary>Performs an two-track operation</summary>
let apply f twoTrackInput =
  match f, twoTrackInput with
  | Success f, Success x -> Success (f x)
  | Failure f, Success _ -> Failure f
  | _, Failure x -> Failure x

let (<*>) = apply

let retrn input =
  Success input

///<summary>Pipe a two-track value into a switch function</summary>
let (>>=) x f =
  bind f x

///<summary>Compose two switches into another switch</summary>
let (>=>) s1 s2 =
  s1 >> bind s2

///<summary>Convert a one-track function into a switch</summary>
let switch f =
  f >> succeed

///<summary>Convert a one-track function into a two-track function</summary>
let map f =
  either (f >> succeed) fail

let traverseCollection fold f seq =
  let cons head tail = head :: tail

  let initState = retrn []
  let folder head tail =
    retrn cons <*> (f head) <*> tail

  fold folder seq initState

let traverseList f list =
  traverseCollection List.foldBack f list

let traverseSeq f seq =
  traverseCollection Seq.foldBack f seq

let traverseArray f array =
  traverseCollection Array.foldBack f array

///<summary>Convert a dead-end function into a one-track function</summary>
let tee f x =
  f x; x

///<summary>Convert a one-track function into a switch with exception handling</summary>
let tryCatch f exceptionHandler x =
  try
    f x |> succeed
  with
  | ex -> exceptionHandler ex |> fail

///<summary>Convert two one-track functions into a two-track function</summary>
let doubleMap successFunc failureFunc =
  either (successFunc >> succeed) (failureFunc >> fail)

///<summary>Add two switches in parallel</summary>
let plus addSuccess addFailure switch1 switch2 x =
  match (switch1 x), (switch2 x) with
  | Success s1, Success s2 -> Success (addSuccess s1 s2)
  | Failure f1, Success _ -> Failure f1
  | Success _, Failure f2 -> Failure f2
  | Failure f1, Failure f2 -> Failure (addFailure f1 f2)
