﻿module Todolist.Console.Command

open LiteDB
open Todolist.Console.Shared
open Todolist.Console.Repositories

type Command =
  | Add
  | Remove
  | Update
  | Complete
  | List
  | All
  | Get

type ConsoleArguments = {
  Command: Command
  TodoId: int
  TodoTitle: string
}

let toList item =
  match item with
  | Success s -> Success [s]
  | Failure f -> Failure f

let execute (args:ConsoleArguments) =
  let repo = TodoRepository.createUsing (new LiteDatabase(@"Filename=C:\Temp\Database.db; Connection=shared"))
  match args.Command with
  | Add -> repo.add args.TodoTitle |> toList
  | Remove -> repo.remove args.TodoId |> toList
  | List -> repo.getOngoing
  | Update -> repo.updateTitle args.TodoId args.TodoTitle |> toList
  | Complete -> repo.updateStatus args.TodoId |> toList
  | All -> repo.getAll
  | Get -> repo.get args.TodoId |> toList
