﻿module Todolist.Console.Repositories

open System
open LiteDB
open Shared
open ValidationTypes
open Model
open RepositoryTypes

module TodoRepository =
  [<AllowNullLiteral>]
  type TodoDto() =
    member val Id = 0 with get, set
    member val Title: string = null with get, set
    member val IsDone = false with get, set
    member val DoneDate: Nullable<DateTime> = Nullable() with get, set
    member val CreationDate = DateTime.Now with get, set

  module private TodoDto =
    let doesExits (todoDto:TodoDto) =
      match isNull todoDto with
      | false -> Success todoDto
      | true -> Failure DatabaseEntryDoesntExists

    let validate (todoDto:TodoDto) =
      let condition =
        (not (isNull todoDto.Title))
        && ((not todoDto.IsDone) || (todoDto.IsDone && todoDto.DoneDate.HasValue))
      match condition with
      | true -> Success todoDto
      | false -> Failure DatabaseEntryIsInvalid

    let private map (todoDto:TodoDto) =
      {
        Id = todoDto.Id
        Title = todoDto.Title
        Status =
          match todoDto.IsDone with
          | true -> Done todoDto.DoneDate.Value
          | false -> Ongoing
        CreationDate = todoDto.CreationDate
      }

    let mapOne (todoDto:TodoDto) =
      try
        todoDto
        |> doesExits
        |> bind validate
        |> Shared.map map
      with
        | :? exn -> Failure DatabaseMappingFailed

  let createUsing (db:ILiteDatabase) : ITodoRepository =

    let collection = db.GetCollection<TodoDto>("todos")

    let getDto (todoId:int) =
      let res = collection.FindById(BsonValue todoId)
      match isNull res with
        | true -> Failure DatabaseEntryDoesntExists
        | false -> Success res

    let updateRow (todoDto:TodoDto) (todoId:int)=
      let todo = TodoDto.mapOne todoDto
      match todo with
      | Failure f -> Failure f
      | Success result ->
        if collection.Update(BsonValue todoId, todoDto) then
          Success result
        else
          Failure DbmsUpdateError

    let repository : ITodoRepository = {
      get = fun todoId ->
        getDto todoId
        |> bind TodoDto.mapOne

      getAll =
        collection.FindAll()
        |> traverseSeq TodoDto.mapOne

      getOngoing =
        collection.Query().Where(fun x -> not x.IsDone).ToEnumerable()
        |> traverseSeq TodoDto.mapOne

      add = fun todoTitle ->
        let dto = TodoDto()
        dto.Title <- todoTitle

        collection.Insert(dto).AsInt32
        |> getDto
        |> bind TodoDto.mapOne

      remove = fun todoId ->
        let todo = getDto todoId |> bind TodoDto.mapOne

        match todo with
        | Failure f -> Failure f
        | Success result ->
          if collection.Delete(BsonValue todoId) then
            Success result
          else
            Failure DbmsDeleteError

      updateTitle = fun todoId todoTitle ->
        let todoDto = getDto todoId
        match todoDto with
        | Failure f -> Failure f
        | Success res ->
          res.Title <- todoTitle
          updateRow res todoId

      updateStatus = fun todoId ->
        let todoDto = getDto todoId
        match todoDto with
        | Failure f -> Failure f
        | Success res ->
          res.IsDone <- not res.IsDone
          if res.IsDone then
            res.DoneDate <- DateTime.Now
          else
            res.DoneDate <- Nullable()
          updateRow res todoId
    }
    repository
