﻿module Todolist.Console.ValidationTypes

type Message =
  | DatabaseEntryIsInvalid
  | DatabaseEntryDoesntExists
  | DatabaseMappingFailed
  | DbmsUpdateError
  | DbmsDeleteError
  | ConsoleArgumentsCouldNotBeMade
  | ConsoleArgumentsCommandDoesNotExist of string
  | ConsoleArgumentsNoArgumentSpecified
